"""
Django settings for openshift project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import imp

ON_OPENSHIFT = False
if 'OPENSHIFT_REPO_DIR' in os.environ:
    ON_OPENSHIFT = True

BASE_DIR = os.path.dirname(os.path.realpath(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
default_keys = { 'SECRET_KEY': 'y9@tlhdhg&t$dd-&$!gb)k2=bvn2+%ej0e6nq!(gjn#sam6$w)' }
use_keys = default_keys
if ON_OPENSHIFT:
    imp.find_module('openshiftlibs')
    import openshiftlibs
    use_keys = openshiftlibs.openshift_secure(default_keys)

SECRET_KEY = use_keys['SECRET_KEY']

# SECURITY WARNING: don't run with debug turned on in production!
if ON_OPENSHIFT:
    DEBUG = False
else:
    DEBUG = True

TEMPLATE_DEBUG = DEBUG

if DEBUG:
    ALLOWED_HOSTS = []
else:
    ALLOWED_HOSTS = ['*']

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'logon',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

# If you want configure the REDISCLOUD
if 'REDISCLOUD_URL' in os.environ and 'REDISCLOUD_PORT' in os.environ and 'REDISCLOUD_PASSWORD' in os.environ:
    redis_server = os.environ['REDISCLOUD_URL']
    redis_port = os.environ['REDISCLOUD_PORT']
    redis_password = os.environ['REDISCLOUD_PASSWORD']
    CACHES = {
        'default' : {
            'BACKEND' : 'redis_cache.RedisCache',
            'LOCATION' : '%s:%d'%(redis_server,int(redis_port)),
            'OPTIONS' : {
                'DB':0,
                'PARSER_CLASS' : 'redis.connection.HiredisParser',
                'PASSWORD' : redis_password,
            }
        }
    }
    MIDDLEWARE_CLASSES = ('django.middleware.cache.UpdateCacheMiddleware',) + MIDDLEWARE_CLASSES + ('django.middleware.cache.FetchFromCacheMiddleware',)


ROOT_URLCONF = 'urls'

if ON_OPENSHIFT:
    WSGI_APPLICATION = 'wsgi.application'

TEMPLATE_DIRS = (
     os.path.join(BASE_DIR,'templates'),
)

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
if ON_OPENSHIFT:
    try:
        #https://forums.openshift.com/django-16-python-33-mysql-no-module-named-mysqldb
        print("IMPORTING - PyMySQL as MySQLdb")
        import pymysql
        pymysql.install_as_MySQLdb()
    except ImportError:
        print("CANT FIND - PyMySQL")
        pass

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(os.environ['OPENSHIFT_DATA_DIR'], 'db.sqlite3'),
            #Use this when you are ready for MySQL, and delete the above sqlite config
            #'ENGINE': 'django.db.backends.mysql',
            #'NAME': 'testdb',
            #'USER': os.environ['OPENSHIFT_MYSQL_DB_USERNAME'],
            #'PASSWORD': os.environ['OPENSHIFT_MYSQL_DB_PASSWORD'],
            #'HOST': os.environ['OPENSHIFT_MYSQL_DB_HOST'],
            #'PORT': '3306',
        }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
    }

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

#https://docs.djangoproject.com/en/1.6/ref/settings/
LOGIN_URL='/'

#####################################
###Static files (CSS, JavaScript, Images, and some HTML pages)

# https://docs.djangoproject.com/en/1.6/howto/static-files/
#This is the path you see in the web browser
STATIC_URL = '/static/'

#This implementation assumes all static files are only in the one static directory
if DEBUG:
    #These are the locations we put static files in DEV/DEBUG (will be auto served by Django)
    STATICFILES_DIRS = (os.path.join(BASE_DIR, '..', 'static'),)
else:
    #Collect/deploy files from STATICFILES_DIRS here, used in PROD (with python manage.py collectstatic)
    #Assumes [Apache] web server will have the <STATIC_URL> bound to this directory (i.e. WSGI)
    STATIC_ROOT = os.path.join(BASE_DIR, '..', 'static')


#####################################
###LOGGING APPENDER HANDLERS
#http://azaleasays.com/2014/05/01/django-logging-with-rotatingfilehandler-error/
#print("Initializing LOGGING in settings.py - if you see this more than once use 'runserver --noreload'")

if ON_OPENSHIFT:
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'verbose': {
                'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
                'datefmt' : "%d/%b/%Y %H:%M:%S"
            },
            'simple': {
                'format': '%(levelname)s %(message)s'
            },
        },
        'handlers': {
            'file': {
                'level': 'DEBUG',
                'class': 'logging.handlers.TimedRotatingFileHandler',
                'filename': os.path.join(os.environ['OPENSHIFT_LOG_DIR'], 'log.txt'),
                'when' : 'midnight',
                'interval' : 1,
                'backupCount' : 7,
                'formatter': 'verbose'
            }
        },
        'loggers': {
            'django': {
                'handlers':['file'],
                'level':'INFO',
                'propagate': True,
            },
            'logon': {
                'handlers': ['file'],
                'level': 'DEBUG',
            },
        }
    }
else:
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'verbose': {
                'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
                'datefmt' : "%d/%b/%Y %H:%M:%S"
            },
            'simple': {
                'format': '%(levelname)s %(message)s'
            },
        },
        'handlers': {
            'file': {
                'level': 'DEBUG',
                'class': 'logging.FileHandler',
                'filename': 'log.txt',
                'formatter': 'verbose'
            },
            'console':{
                'level':'DEBUG',
                'class':'logging.StreamHandler',
                'formatter': 'verbose'
            }
        },
        'loggers': {
            'django': {
                'handlers':['file'],
                'level':'DEBUG',
                'propagate': True,
            },
            'logon': {
                'handlers': ['file', 'console'],
                'level': 'DEBUG',
            },
        }
    }

#####################################
### Add your APP specific configuration variables here
