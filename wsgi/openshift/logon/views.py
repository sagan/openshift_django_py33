from django.http import HttpResponse
import json
import logging
import logon.utility as logonUtility
import urllib
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.db import transaction


#Much of this logic and APi came from here
#https://docs.djangoproject.com/en/1.7/topics/auth/default/

# Get an instance of a logger
logger = logging.getLogger(__name__)


def isUserLoggedIn(request):
    #ts = request.GET['ts']
    if request.user.is_authenticated():
        data = {'result':True, 'username':request.user.username}
    else:
        data = {'result':False}

    data = json.dumps(data)
    return HttpResponse(data, content_type="application/json")


@csrf_exempt
def doLogin(request):
    logger.info(logonUtility.formatLogMessage(request, "Login Attempt"))
    #first logout any current users
    logout(request)

    #Reset the cookies to the default future datetime for both session and csrf cookies
    request.session.set_expiry(None)
    request.session.clear_expired()

    #JSON is in BODY payload as RAW not URL-Form-Encoded
    #username = request.POST['_s_email']
    #password = request.POST['_s_password']

    body = json.loads(request.body.decode("utf-8"))
    username = body['_s_username']
    password = body['_s_password']

    #Do the actual AUTH, this will set the csrf cookie
    user = authenticate(username=username, password=password)

    #Determine if user info was valid
    if user is not None:
        if user.is_active:
            login(request, user)
            data = {'result':True, 'username':user.username}
            logger.info(logonUtility.formatLogMessage(request, "Login Successful"))
        else:
            data = {'result':False}
            logger.info(logonUtility.formatLogMessage(request, "Login Failed, user is inactive"))
    else:
        data = {'result':False}
        logger.info(logonUtility.formatLogMessage(request, "Login Failed"))

    data = json.dumps(data)
    return HttpResponse(data, content_type="application/json")


def doLogout(request):
    logout(request)
    data = {'result':True}
    data = json.dumps(data)
    return HttpResponse(data, content_type="application/json")


@csrf_exempt
def doRegister(request):
    username = request.POST['username']
    password = request.POST['password']
    email = username
    user = User.objects.create_user(username, email, password)

    if user is not None:
        data = {'result':True}
    else:
        data = {'result':False}

    data = json.dumps(data)
    return HttpResponse(data, content_type="application/json")


def setUserPassword(request):
    password1 = request.POST['password1']
    password2 = request.POST['password2']

    if (password1 == password2):
        request.user.set_password('new password')
        data = {'result':True}
    else:
        data = {'result':False}

    data = json.dumps(data)
    return HttpResponse(data, content_type="application/json")
