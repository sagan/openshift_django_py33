import unittest
from django.test import Client
import json
import django

#http://django.readthedocs.org/en/latest/releases/1.7.html
#Look for AppRegistryNotReady exception
django.setup()
#Also set ENV-VAR DJANGO_SETTINGS_MODULE=fbpoll.settings

# Create your tests here.
#https://docs.djangoproject.com/en/1.7/topics/testing/tools/

class SimpleTest(unittest.TestCase):
    def setUp(self):
        # Every test needs a client.
        self.client = Client()

    def test_user(self):
        body = json.dumps({'_s_username': 'qa', '_s_password': 'qaqa'})
        responseLoginBad = self.client.post('/user_login/', body, 'application/json')
        responseLoginBadData = json.loads(responseLoginBad.content.decode("utf-8"))
        self.assertEqual(responseLoginBadData['result'], False)

        body = json.dumps({'_s_username': 'qa', '_s_password': 'qa'})
        responseLoginGood = self.client.post('/user_login/', body, 'application/json')
        responseLoginGoodData = json.loads(responseLoginGood.content.decode("utf-8"))
        self.assertEqual(responseLoginGoodData['result'], True)

        responseUser = self.client.get('/user/')
        responseUserData = json.loads(responseUser.content.decode("utf-8"))
        self.assertEqual(responseUserData['username'], 'qa')

        responseLogout = self.client.get('/user_logout/')
        self.assertEqual(responseLogout.status_code, 200)
        responseLogoutData = json.loads(responseLogout.content.decode("utf-8"))
        self.assertEqual(responseLogoutData['result'], True)

