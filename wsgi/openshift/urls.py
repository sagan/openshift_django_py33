from django.conf.urls import patterns, include, url
from django.contrib import admin
from logon import views as logon_view

#Allows admin app to see all your apps, maybe disable during production
admin.autodiscover()

#Base App URLs
#http://www.djangobook.com/en/2.0/chapter08.html
urlpatterns = patterns('',
    url(r'^$', 'views.index', name='index'),
    # This (django.contrib.staticfiles.views) only works in DEBUG=TRUE
    #url(r'^$', 'django.contrib.staticfiles.views.serve', kwargs={'path': 'index.html'}),
    url(r'^home$', 'views.home', name='home'),
    #http://stackoverflow.com/questions/4845239/how-can-i-disable-djangos-admin-in-a-deployed-project-but-keep-it-for-local-de
    url(r'^admin/', include(admin.site.urls)),
    # url(r'^$', 'openshift.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
)

#Logon App URLs
urlpatterns += patterns('',
    url(r'^user/$', logon_view.isUserLoggedIn),
    url(r'^user_login/$', logon_view.doLogin),
    url(r'^user_logout/$', logon_view.doLogout),
    url(r'^user_register/$', logon_view.doRegister),
    url(r'^user_password/$', logon_view.setUserPassword),
)

#Add you next app here
urlpatterns += patterns('',
)
