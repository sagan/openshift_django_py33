Openshift Deployment
====================
Some details when starting from a blank slate.


Make a new app in OpenShift
---------------------------
	Go to the OpenShift Admin WebUI
	Add a New App
	Pick Django and Python3
	Scale to multiple web servers (for production)


Use the git repo from our source code
-------------------------------------
    https://bitbucket.org/sagan/relationship.git
    Make the repo publicly available for a short time; there may be some private way if you really need it
    Update the settings.py file, to NOT use MySQL (i.e. use sqlite).
    Push the repo
    Now finally, ADD the new App; wait 2-3 minutes


If Deployment is Successful
---------------------------
    Add the MySQL cartridge from the WebUI (i.e. 1 gear)
    Go look for the app-root/data/CREDENTIALS file
    Update the settings.py to use MySQL (i.e. DB_NAME)
    Push the repo
    It will auto-create all the tables from Django migrate DB (i.e. South)
    There is still no user in the DB


If Deployment is NOT Successful
---------------------------
    Look for the static3==0.5.1 dependency (WARNING: version 0.6.0 deosnt work, some ASCII encoding error will occur)
    Make sure all action_hooks are rwx (chmod 755)
    See if the CREDENTIALS has anything in it, to see if secure_db.py completed ok
    Try to run the deploy script manually


Add the Superuser
-----------------
    python manage.py createsuperuser
    use the password from the CREDENTIALS file


Setup Putty with Port Forwarding to the DB
------------------------------------------
    SSH in (dont forget to setup AUTH keys, so no password is needed)
    > env | grep MYSQL
    Look at the values
    Add a port forward from L3306
    Setup MySql client to point to 127.0.0.1:3306
    use the password from the CREDENTIALS file
    Login and see the tables, and 1 user


Misc
----
    Add bookmarks to the web page
    Setup DNS to CNAME
