Django 1.6 and Python 3 on OpenShift
=========================================================

This git repository helps you get up and running quickly w/ a Django 1.6 and Python 3.3 installation on OpenShift. 


Details
-------
	Deploy: Openshift Cloud
	Requires: Python 3.3, Django 1.6, MySQL 5.5
	Recommendations for local Dev: PyCharm, pip


More Details
------------
	HTML
	Bootstrap
	Font-Awesome
	AngularJS Controllers
	REST/JSON via Django Python API
	MySQL


PyCharm
-------
    When you open the project, set the 'openshift' dir as 'mark directory as source root'
    Use RUN configuration with: Script="<path to your local reo>\wsgi\openshift\manage.py", Parameters="runserver"


Scripts you might need to run during development
------------------------------------------------
    https://docs.djangoproject.com/en/1.6/intro/tutorial01/
    python manage.py runserver
    python manage.py migrate
    python manage.py syncdb
    python manage.py startapp my_next_app
    python manage.py changepassword admin


Database
--------
    By default it uses sqlite
    Once you get a successful deployment to the OpenShift cloud, you can switch to MySQL
    See the settings.py for an existing example


Misc
----
    Home page - http://127.0.0.1:8000/
    Admin site - http://127.0.0.1:8000/admin/
    Superuser credentials (DEV): admin/pass (CHANGE THIS ASAP!!!)
    Superuser credentials (OpenShift): look in file ${OPENSHIFT_DATA_DIR}/CREDENTIALS, after cloud deployment
